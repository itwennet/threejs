## 简介<br/> 
Threejs基础实例Demo代码，这个例子就是我的学习Demo，主要是参考了官网的例子以及效果演示中提到的那两个列子，Demo例子大体上已经封装实现了加载OBJ、FBX、原生对象、2D、3Dlabel等，封装对象鼠标事件的监听，镜头动画封装，基本上满足业务的开发

## 仓库地址<br/> 
国外：https://github.com/huanzi-qch/threejs<br/> 
国内：https://gitee.com/huanzi-qch/threejs<br/> 

## 运行效果图<br/> 

地球<br/>
![](https://huanzi-qch.gitee.io/file-server/images/threejs-1.png) <br/>

双击地球“穿越云海”过渡动画<br/>
![](https://huanzi-qch.gitee.io/file-server/images/threejs-2.png) <br/>

进入到园区场景<br/>
![](https://huanzi-qch.gitee.io/file-server/images/threejs-3.png) <br/>

## 前往博客查看详情<br/> 
具体介绍请看我的博客[《TypeScript进阶开发——ThreeJs基础实例，从入坑到入门》](https://www.cnblogs.com/huanzi-qch/p/11413739.html ) <br/>

## [AD广告位](https://huanzi-qch.gitee.io/file-server/ad/adservice.html) （长期招租，如有需要请私信）<br/> 
[【基塔后台】免费后台管理系统，低代码快速搭建管理后台](https://www.jeata.com/?hmsr=promotion&hmpl=huanzi-qch) <br/>
<br/>
[【阿里云】阿里云最全的优惠活动聚集地！](https://www.aliyun.com/activity?userCode=ckkryd9h) <br/>
[【腾讯云】腾讯云当前最新优惠活动专区！](https://cloud.tencent.com/act/cps/redirect?redirect=11447&cps_key=e1c9db729edccd479fc902634492bf53) <br/>
<br/>

## QQ群<br/>
有事请加群，有问题进群大家一起交流！<br/>
![](https://huanzi-qch.gitee.io/file-server/images/qq.png) 

## 捐献<br/>
相应的资金支持能更好的持续项目的维护和开发，如果喜欢这个项目，请随意打赏！

| 支付宝 | 微信 |
|  ----  | ----  |
| <img src="http://huanzi-qch.gitee.io/file-server/images/zhifubao.png"  width="150"> | <img src="http://huanzi-qch.gitee.io/file-server/images/weixin.png" width="150"> |

